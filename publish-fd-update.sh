#!/bin/bash

set -e
unalias rm||true
echo "Welcome to F-Droid version publisher!"
echo 
echo "Press Ctrl+C at any time to abort."
echo


#echo "+git pull weblate master"
#git remote add weblate https://hosted.weblate.org/git/superfreezz/superfreezz/ 2>/dev/null || true
#git pull weblate master
#echo


echo
echo "+git status"
git status
echo

echo -n 'new versionCode="..." (integer) '
read vCode
echo -n 'new versionName="..." (string) '
read vName

echo "Attatch testing device and"
echo "change versionCode and versionName in AndroidManifest.xml, then press enter"
read
echo "+git add **/AndroidManifest.xml"
git add **/AndroidManifest.xml
git add app/src/main/java/tool/fff/profilepicturegenerator/MainActivity.kt

# Generate release notes, let the user edit them and move them to the fastlane changelogs directory
git log $(git describe --tags --abbrev=0)..HEAD --oneline --no-decorate --no-color | cut -d' ' -f2- > F-Droid-new-version-RELEASE-NOTES.txt
nano F-Droid-new-version-RELEASE-NOTES.txt 
echo "cp F-Droid-new-version-RELEASE-NOTES.txt ./fastlane/metadata/android/en-US/changelogs/${vCode}.txt"
cp F-Droid-new-version-RELEASE-NOTES.txt "./fastlane/metadata/android/en-US/changelogs/${vCode}.txt"

echo "+git add fastlane/metadata/android/en-US/changelogs"
git add fastlane/metadata/android/en-US/changelogs/



# Fastlane:
adb push  ic_launcher2-raw.png /storage/emulated/0/DCIM/Camera/ic_launcher2-raw.png
echo "rm /storage/emulated/0/Pictures/FridaysForFuture-Profilbild.jpg"|adb shell || true
rm -f ic_launcher2.png ic_launcher2.jpg || true
./gradlew assembleDebug assembleAndroidTest
fastlane screengrab
adb pull /storage/emulated/0/Pictures/FridaysForFuture-Profilbild.jpg ./ic_launcher2.jpg
thunar fastlane/metadata/android/en-DE/images/phoneScreenshots/
echo "Verify screenshots, use new app icon in Android Studio, press enter"
read
rm fastlane/metadata/android/en-DE/images/phoneScreenshots/verify*
rm fastlane/metadata/android/de-DE/images/phoneScreenshots/*||true
mv fastlane/metadata/android/en-DE/images/phoneScreenshots/* fastlane/metadata/android/de-DE/images/phoneScreenshots
git add app/src/main/res/
git add fastlane/metadata/android/de-DE/
rm fastlane/metadata/android/screenshots.html
git add ic_launcher*
git add app/src/main/ic_launcher-web.png


echo "+git commit -m 'Bump version'"
git commit -m "Bump version to $vName" || true

echo "+git tag -a v${vName} -F F-Droid-new-version-RELEASE-NOTES.txt"
git tag -a "v${vName}" -F F-Droid-new-version-RELEASE-NOTES.txt

echo "Press enter to publish the new version."
#read
echo "+git pull && git push && git push --tags"
git pull && git push && git push --tags

echo "+rm F-Droid-new-version-RELEASE-NOTES.txt"
rm F-Droid-new-version-RELEASE-NOTES.txt

