/*
 *     This file is part of the program "Fridays for Future Profilbildgenerator".
 *
 *     Copyright (C) 2019 Hocuri
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package tool.fff.profilepicturegenerator

import android.content.Intent
import android.net.Uri
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.scrollTo
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import androidx.test.rule.ActivityTestRule
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import tools.fastlane.screengrab.Screengrab
import tools.fastlane.screengrab.UiAutomatorScreenshotStrategy

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
@LargeTest
class ExampleInstrumentedTest {

	//@ClassRule
	//val localeTestRule = LocaleTestRule()


	@Before
	fun setUp() {
		Screengrab.setDefaultScreenshotStrategy(UiAutomatorScreenshotStrategy())
	}

	@get:Rule
	val activityRule = ActivityTestRule(MainActivity::class.java)

	/*@Test
	fun backOnNull() {
		val a = activityRule.activity
		onView(withId(R.id.button_generate)).perform(click())
		Thread.sleep(5000)
		//val a = getCurrentActivity() as MainActivity
		a.runOnUiThread {
			a.onActivityResult(
				a.FILECHOOSER_RESULTCODE,
				Activity.RESULT_OK,
				Intent()
			)
		}
		Thread.sleep(2000)
		onView(withText("Aktuelles Profilbild auswählen")).check(matches(isDisplayed()))

	}*/

	@Test
	fun walkThrough() {
		val a = activityRule.activity
		skipShowingFileChooser = true
		Espresso.closeSoftKeyboard()
		Thread.sleep(1000)
		Screengrab.screenshot("Screenshot1")

		a.savedResultIntent = Intent().setData(Uri.parse("file:///storage/emulated/0/DCIM/Camera/ic_launcher2-raw.png"))

		onView(withId(R.id.button_generate)).perform(scrollTo(), click())

		onView(withId(R.id.button_done)).waitForElementUntilDisplayed()
		Thread.sleep(1000)
		Screengrab.screenshot("Screenshot2")

		onView(withId(R.id.button_done)).perform(click())

		onView(withId(R.id.button_done_support)).waitForElementUntilDisplayed()
		Screengrab.screenshot("verify1")
		onView(withId(R.id.button_done_support)).perform(click())

		openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)
		onView(withText(a.getString(R.string.about))).perform(click())
		Screengrab.screenshot("verify2")
	}


	private fun ViewInteraction.waitForElementUntilDisplayed() {
		repeat(30) {
			try {
				check(matches(isDisplayed()))
				return
			} catch (e: Exception) {
				Thread.sleep(1000)
			}
		}
		check(matches(isDisplayed()))
	}
}

