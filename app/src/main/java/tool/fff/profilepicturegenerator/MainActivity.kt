/*
 *     This file is part of the program "Fridays for Future Profilbildgenerator".
 *
 *     Copyright (C) 2019 Hocuri
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package tool.fff.profilepicturegenerator

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Color
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.*
import kotlin.reflect.KClass


val dateOfNextDemo = GregorianCalendar(2020, Calendar.SEPTEMBER, 25)
const val infoUrl = "https://fridaysforfuture.de/keingradweiter/"
const val extraHint = ""
const val extraTitle = ""


var skipShowingFileChooser = false // Needed for tests

var renderedBitMap: Bitmap? = null
var renderedBitMapUri: Uri? = null

class MainActivity : AppCompatActivity() {

	var savedResultIntent: Intent? = null
	private var currentDialog: Dialog? = null
	@Suppress("PrivatePropertyName")
	private val FILECHOOSER_RESULTCODE = 1
	private val handler = Handler()

	@SuppressLint("ObsoleteSdkInt")
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)


		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && BuildConfig.DEBUG) {
			// Uncomment to test the ShareRenderedPictureActivity:
			//renderedBitMap = getDrawable(R.mipmap.ic_launcher)!!.toBitmap(200, 200)
		}

		if (renderedBitMap != null) {
			switchTo(ShareRenderedPictureActivity::class)
			return
		}

		setContentView(R.layout.activity_main)
		setSupportActionBar(toolbar)

		@Suppress("ConstantConditionIf")
		if (dateOfNextDemo < GregorianCalendar()) {
			text_title.text =
				"Die Demo vom ${SimpleDateFormat.getDateInstance()
					.format(dateOfNextDemo.time)} ist vorbei. Danke an alle, die gekommen sind!\n\n" +
						"Ca. einen Monat vor der nächsten Demo gibt es ein Update für diese App, " +
						"sodass sie das neue Profilbild generiert. Bis dahin kannst du dich auf fridaysforfuture.de " +
						"über Neuigkeiten informieren.\n\nDu kannst diese App trotzdem ausprobieren, aber es kann sein, dass sie nicht mehr richtig funktioniert."
			text_title.setTextColor(Color.RED)
		} else if (extraTitle != "") {
			text_title.text = text_title.text.toString() + extraTitle
		}

		@Suppress("ConstantConditionIf")
		if (extraHint != "") {
			extra_hint.visibility = View.VISIBLE
			extra_hint.text = extraHint
		}

		button_generate.setOnClickListener {
			showFileChooser()
			// The webview can't be used a second time (I did not find out why).
			// So, disable the button to prevent an accidental second click (we will recreate()
			// or finish() this activity anyway)
			button_generate.isEnabled = false
		}

	}

	override fun onDestroy() {
		super.onDestroy()
		Log.i(TAG, "onDestroy")
		handler.removeCallbacksAndMessages(null)
		currentDialog?.dismiss()
	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		menuInflater.inflate(R.menu.menu_main, menu)
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
		R.id.action_about -> {
			switchTo(AboutActivity::class)
			true
		}
		else -> super.onOptionsItemSelected(item)
	}


	private fun showFileChooser() {
		Log.i(TAG, "showFileChooser")

		if (!skipShowingFileChooser) {
			val i = Intent(Intent.ACTION_GET_CONTENT)
			i.addCategory(Intent.CATEGORY_OPENABLE)
			i.type = "image/*"
			startActivityForResult(
				Intent.createChooser(i, "Profilbild auswählen"),
				FILECHOOSER_RESULTCODE
			)
		}
	}


	// ============================================================================================================================
	// This is called when the user chose a picture:
	// It will tell this to the website and click the "Download" button once a second
	// until the picture can be downloaded
	// ============================================================================================================================
	@Suppress("DEPRECATION")
	public override fun onActivityResult(
		requestCode: Int, resultCode: Int,
		intent: Intent?
	) {
		super.onActivityResult(requestCode, resultCode, intent)
		//Log.v(TAG, "Intent: $intent, data: ${intent?.data}")
		if (requestCode == FILECHOOSER_RESULTCODE) {

			Log.i(TAG, "picture was chosen")
			if (intent == null || resultCode != Activity.RESULT_OK || intent.data == null) {
				recreate()
				return
			}
			val chosenImage = MediaStore.Images.Media.getBitmap(this.contentResolver, intent.data!!)
			renderedBitMap = addProfilePicture(this, chosenImage)
			renderedBitMapUri = null
			switchTo(ShareRenderedPictureActivity::class)
		}
	}


	// ============================================================================================================================
	// Finally, the helper functions:
	// ============================================================================================================================

}

fun Activity.switchTo(activity: KClass<out Activity>) {
	startActivity(Intent(this, activity.java))
	finish()
}

fun Context.viewInBrowser(uri: String) {
	val intent = Intent(
		Intent.ACTION_VIEW,
		Uri.parse(uri)
	)
	if (intent.resolveActivity(packageManager) != null)
		startActivity(intent)
	else
		Toast.makeText(
			this,
			"No browser found. Maybe you should install the app 'Firefox'?",
			Toast.LENGTH_LONG
		).show()
}

fun addProfilePicture(context: Context, bitmap: Bitmap): Bitmap? {
	val kgw = getBitmapFromAsset(context, "kgw.png")
	Log.e(TAG, "alpha: ${kgw.hasAlpha()}")
	return combineImages(bitmap, kgw)
}

fun getBitmapFromAsset(context: Context, filePath: String): Bitmap {
	val assetManager = context.assets
	val istr: InputStream
	istr = assetManager.open(filePath)
	val opts = BitmapFactory.Options()
	opts.inPreferredConfig = Bitmap.Config.ARGB_8888
	return BitmapFactory.decodeStream(istr, null, opts)!!
}

fun combineImages(original: Bitmap, overlay: Bitmap): Bitmap? {
	val result = Bitmap.createBitmap(overlay.width, overlay.height, Bitmap.Config.ARGB_8888)
	val comboImage = Canvas(result)
	val cropped = ThumbnailUtils.extractThumbnail(original, overlay.width, overlay.height)
	comboImage.drawBitmap(cropped, 0f, 0f, null)
	comboImage.drawBitmap(overlay, 0f, 0f, null)
	return result
}

const val TAG = "FfF-profile-code"