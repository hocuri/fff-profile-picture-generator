/*
 *     This file is part of the program "Fridays for Future Profilbildgenerator".
 *
 *     Copyright (C) 2019 Hocuri
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package tool.fff.profilepicturegenerator

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

const val MAX_LOG_LENGTH = 200000

fun Context.sendFeedbackEmail() {

	var logs = getLogs()
	if (logs.length > MAX_LOG_LENGTH) { // An intent must not contain more than 1MB
		logs = "Truncated\n" + logs.substring(logs.length - MAX_LOG_LENGTH, logs.length - 1)
	}
	val message = "Hier kannst du dein Feedback schreiben. Weiter unten stehen ein paar Infos für mich " +
			"darüber, wie du diese App benutzt hast, du kannst sie aber auch einfach löschen." +
			"\n\n\nAndroid ${android.os.Build.VERSION.RELEASE}\n $logs"


	val subject = "Feedback Profilbildgenerator v${BuildConfig.VERSION_NAME}"
	val email = "fff-profilbild-generator@web.de"

	var sharingIntent = Intent(
		Intent.ACTION_SENDTO, Uri.fromParts(
			"mailto", email, null
		)
	)
	sharingIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
	sharingIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
	sharingIntent.putExtra(Intent.EXTRA_TEXT, message)

	if (sharingIntent.resolveActivity(packageManager) == null) {
		Log.w(TAG, "No email client found to send crash report")
		sharingIntent = Intent(Intent.ACTION_SEND)
		sharingIntent.type = "text/plain"
		sharingIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
		sharingIntent.putExtra(Intent.EXTRA_TEXT, message)
		sharingIntent.putExtra(Intent.EXTRA_EMAIL, email)
	}

	val chooser = Intent.createChooser(sharingIntent, "Senden über...")
	chooser.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
	chooser.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK)

	startActivity(chooser)
}

private fun getLogs(): String {
	/* Only for testing:
	val b = StringBuilder();
	repeat(1000000) {
		b.append('a')
	}
	return b.toString()*/
	val result = try {
		val process = Runtime.getRuntime().exec("logcat -d")
		val bufferedReader = BufferedReader(
			InputStreamReader(process.inputStream)
		)

		"Version " + BuildConfig.VERSION_NAME + bufferedReader.use { it.readText() }

	} catch (e: IOException) {
		Log.e(TAG, "Could not get logs (???)")
		""
	}
	//return result.lines().filter { it.contains("FfF") || it.contains("W/") || it.contains("E/") }
	return result
}
