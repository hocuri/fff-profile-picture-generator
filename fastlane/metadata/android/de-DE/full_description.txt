Diese App erleichtert es dir, den Profilbild-Generator von fridaysforfuture.de zu benutzen: Wähle dein aktuelles Profilbild aus und du bekommst es mit einem Hinweis auf den nächsten Klimastreik zurück. 

Dann kannst du es direkt mit nur einem Fingertipp als dein Profilbild in verschiedenen Chat-Apps setzen und so deine Freunde auf die Demo aufmerksam machen.

Ich selbst, der Autor dieser App, bin kein Mitglied des Fridays-for-Future-Organisationsteams, aber ein begeisterter Anhänger. Ich hoffe, mit dieser App dazu beizutragen, dass mehr junge Menschen den Profilbildgenerator benutzen und dadurch letzten Endes mehr Leute zum nächsten Streik kommen.

"Why should I be studying for a future that soon may be no more, when no one is doing anything to save that future?"
- Greta Thunberg, 16-jährige Klima-Aktivistin im Streik
